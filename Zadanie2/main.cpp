#include <iostream>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <string>

using namespace std;

//funckja wypelnia tablice losowymi wartosciami z przedia�u od 1 do x
void wypelnianie_losowe (int tab[], int iRozmiar, int x)
{
  srand (time(NULL));
  for (int i=0; i<iRozmiar; i++)
    tab[i]=rand()%x+1;
}

//funcja wyswietla zawartosc tablicy
void wyswietl_zawartosc (int tab[], int iRozmiar)
{
      for (int i=0; i<iRozmiar; i++)
        cout << tab[i] << " ";
}

//funckja zwaraca maksywmalna wartosc w tablicy
int maksymalna_wartosc (int tab[], int iRozmiar)
{
  int maksymalna=0; //zmienna przechowuje najwiekasza wartosc
      for (int i=0; i<iRozmiar; i++)
        if (tab[i]>=maksymalna) maksymalna=tab[i];
  return maksymalna;
}

//funkcja zapisuje zawartosc tablicy do pliku tekstowego
void zapis_tekstowy (int t[], int iRozmiar)
{
    fstream plik;
    plik.open("plik_tekstowy.txt",ios::out);
    for (int i=0; i<iRozmiar; i++)
        plik << t[i] << " ";
    cout << "Zapisano!";
    plik.close();
}

//funkcja wczytuje zawartosc pliku tekstowego
void odczyt_tekstowy (int t[], int iRozmiar)
{
    fstream plik;
    int bufor[100];
    plik.open("plik_tekstowy.txt",ios::in);
    if(plik.good()==false)
        cout << "Nie mozna odczytac pliku.";

    int i=0;
    while (!plik.eof())
    {
        plik >> bufor[i];
        i++;
    }

    for (int i=0; i<iRozmiar; i++)
        t[i]=bufor[i];

    plik.close();
}

void zapis_binarny (int t[], int iRozmiar)
{
    ofstream plik;
    plik.open("plik_binarny.bin", ios::binary);
    plik.write(( const char * ) & t, sizeof t);
    plik.close();
}

void odczyt_binarny (int t[], int iRozmiar)
{
        fstream plik;
        string p_linia;

        plik.open("plik_binarny.bin", ios::in | ios::binary);
        if(plik.good()==false)
            cout << "Nie mozna odczytac pliku.";

        int i=0;

        while(!plik.eof())
        {
            getline(plik,p_linia);
            t[i] = atoi(p_linia.c_str());
            i++;
        }

}


int main()
{
    int tab[100];
    int iRozmiar = 100;
    int x;



    char option;
        do{
            cout << endl;
            cout << "==== MENU GLOWNE ===" << endl << endl;
            cout << "1.Wypelnij tablice losowymi wartosciami od 1 do ..." << endl;
            cout << "2.Wyswietl zawartosc tablicy." << endl;
            cout << "3.Znajdz wartosc maksymalna tablicy." << endl << endl;
            cout << "4.Zapisz tablice do pliku teksotwego." << endl;
            cout << "5.Wczytaj tablice z pliku tekstowego." << endl << endl;
            cout << "6.Zapisz tablice do pliku binarnego." << endl;
            cout << "7.Wczytaj tablice z pliku binarnego." << endl << endl;
            cout << "====================" << endl;
            cout << "0.Wyjscie" << endl;
            cout << "Podaj opcje: ";
            cin >> option;
            cout << endl;

        switch (option)
            {
            case '1':
                cout << "Do jakiej liczby wypelnic? : ";
                cin >> x;
                wypelnianie_losowe(tab,iRozmiar,x);
			break;

            case '2':
                wyswietl_zawartosc(tab,iRozmiar);
			break;

            case '3':
                cout << "Maksymalna wartosc to: " << maksymalna_wartosc(tab,iRozmiar) << endl;
			break;

			case '4':
			    zapis_tekstowy(tab,iRozmiar);
            break;

            case '5':
                odczyt_tekstowy(tab,iRozmiar);
            break;

            case '6':
                zapis_binarny(tab,iRozmiar);
            break;

            case '7':
                odczyt_binarny(tab,iRozmiar);
            break;

            }

        } while (option != '0');


    return 0;
}
