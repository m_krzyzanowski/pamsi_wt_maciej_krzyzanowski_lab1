#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

//funckja wypelnia tablice losowymi wartosciami od 1 do x(parametr funcji)
void wypelnianie_losowe (int **tab, int n, int m, int x)
{
    srand(time(NULL));
    for (int i=0; i<n; i++)
    {
      for (int j=0; j<m; j++)
        tab[i][j]=rand()%x+1;
    }
}

//funcja wyswietla zawartosc tablicy
void wyswietl_zawartosc (int **tab, int n, int m)
{
  for (int i=0; i<n; i++)
    {
      for (int j=0; j<m; j++)
	cout << tab[i][j] << ' ';

      cout << endl;
    }
}

//funckja zwaraca maksywmalna wartosc w tablicy
int maksymalna_wartosc (int **tab, int n, int m)
{
  int maksymalna=tab[0][0]; //zmienna przechowuje najwiekasza wartosc
  for (int i=0; i<n; i++)
    {
      for (int j=0; j<m; j++)
        if (tab[i][j]>=maksymalna) maksymalna=tab[i][j];
    }
  return maksymalna;
}

int main()
{
    int n, m, x;
    cout << "Podaj ilosc wierszy: ";
    cin >> n;
    cout << "Podaj ilosc kolumn: ";
    cin >> m;
    int **tab = new int *[n];
    for (int i=0; i<n; i++)
            tab[i] = new int [m];

    char option;
        do{
            cout << endl;
            cout << "==== MENU GLOWNE ===" << endl;
            cout << "1.Wypelnij tablice losowymi wartosciami od 1 do x." << endl;
            cout << "2.Wyswietl zawartosc tablicy." << endl;
            cout << "3.Znajdz wartosc maksymalna tablicy." << endl;
            cout << "0.Wyjscie" << endl;
            cout << "Podaj opcje:";
            cin >> option;
            cout << endl;

        switch (option)
            {
            case '1':
                cout << "Podaj od 0 do jakiej liczby wylosowac: ";
                cin >> x;
                wypelnianie_losowe(tab,n,m,x);
			break;

            case '2':
                wyswietl_zawartosc(tab,n,m);
			break;

            case '3':
                cout << "Maksymalna wartosc to: " << maksymalna_wartosc(tab,n,m) << endl;
			break;
            }

        } while (option != '0');

    //Usuwanie tablicy
    for (int i=0; i<n; i++)
        delete [] tab[i];
    delete [] tab;


    return 0;
}
