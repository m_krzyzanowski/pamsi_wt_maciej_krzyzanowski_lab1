#include <cstdlib>
#include <iostream>
#include <string>
#include <windows.h>


using namespace std;

bool jestPal(string testStr)
{
    int dlugosc = testStr.length();

    if (dlugosc==0 || dlugosc==1) return true;
    if (testStr.front() == testStr.back())
    {
        testStr.erase(testStr.begin());   //usuniecie pierwszego znaku
        testStr.pop_back();               //usuniecie ostatniego znaku
        return jestPal(testStr);
    }
    else return false;
}

int main()
{
    cout << "=== PALINDROMEN ===" << endl;
    string palind;
    cout << "Podaj spowo ktore sprawdzic: " << endl;
    cin >> palind;
    if(jestPal(palind))
        cout << "To jest palindromen." << endl;
    else cout << "To NIE jest palindromen." << endl;
    return 0;
}

