#include <iostream>

using namespace std;


int Potega(int x, int p)
{
    if (p==0) return 1;
    else return x*Potega(x,p--);
}

int Silnia(int x)
{
    if (x==0) return 1;
    else return x*Silnia(x--);
}


int main()
{
    int p,w,s;
    cout << "=== Potegowanie ===" << endl;
    cout << "Podaj podstawe p.: ";
    cin >> p;
    cout << "Podaj wykladnik p.:";
    cin >> w;
    cout << "Wynik potegowania: " << Potega(p,w) << endl << endl;
    cout << "=== Silnia ===" << endl;
    cout << "Podaj liczbe: ";
    cin >> s;
    cout << Silnia(s) << endl;
    return 0;
}
